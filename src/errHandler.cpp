/*
 * =====================================================================================
 *
 *       Filename:  errHandler.cpp
 *
 *    Description:  Implements the error handler class
 *
 *        Version:  1.0
 *        Created:  12/11/2014 12:49:37 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Nilangshu Bidyanta (), nbidyanta@gmail.com
 *   Organization:
 *
 * =====================================================================================
 */

#include "errHandler.h"

using namespace std;

ExitException::ExitException()
{
	// Empty Constructor
}

ExitException::ExitException(const char* msg, int line,
							 const char* file, const char* func)
{
	what_arg = "@"+to_string(line)+" of "+func+"() in "+file+": "+msg;
	what_arg = string("@") + file + ":" + func + "():" + to_string(line) + " -\n\t" + msg;
}

const char* ExitException::what() const noexcept
{
	return what_arg.c_str();
}
