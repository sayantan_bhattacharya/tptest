/*
 * =====================================================================================
 *
 *       Filename:  animation.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Wednesday 03 June 2015 11:49:43  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/animation.h"

Animation::Animation()
{
    numFrames = 0;
    curFrame = 0;
    timeElapsed = 0.0;
    loop = true;
}

Animation::~Animation()
{
    /* (w) Default destructor - code will be added later */
}

/* (w) function bodies go here */

void Animation::addFrame(const sf::IntRect &inFrame)
{
    mframes.push_back(inFrame);
    numFrames++;
}

sf::IntRect Animation::getFrame(size_t frameNum)
{
    if(frameNum >= mframes.size())
        throw EE("Frame Number is out of bounds");
    else
        return mframes[frameNum];
}

sf::IntRect Animation::getCurFrame()
{
    return mframes[curFrame];
}

void Animation::setDuration(float desDuration)
{
    duration = desDuration;
}

void Animation::updateFrame(float dt)
{
    float timePerAnimFrame = duration / numFrames;
    if((timeElapsed + dt) >= timePerAnimFrame)
    {
        if(loop)
            curFrame = (curFrame + 1) % numFrames;
        else
            curFrame = ((curFrame + 1) % numFrames) ? numFrames - 1 : curFrame + 1;
        timeElapsed = 0.f; /* (w) reset the time elapsed */
    }
    else
        timeElapsed += dt;
}

void Animation::setLooping(bool l)
{
    loop = l;
}

void Animation::resetAnimation()
{
    /* (w) Reset everything */

    curFrame = 0;
    timeElapsed = 0.f;
}
