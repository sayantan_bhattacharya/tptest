/*
 * =====================================================================================
 *
 *       Filename:  titleState.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Saturday 03 January 2015 05:34:15  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/titleState.h"
#include <iostream>

using namespace std;

TitleState::TitleState(StateManager &stateMgr)
{
    this->stMgr = &stateMgr;
    LOG<<INFO<<"Title State initiated\n";
    /* (w) call the load texture function */
    loadTexture();

    /* (w) initialize the increment and decrement flag to false */
    inc = true;
    dec = !inc;
    loadMenu = false;
    displayName = loadMenu;
    identity = TITILESTATE;

    // set the alpha value
    alphaVal = 0;

    loadFont("./res/menuStateRes/testFont.ttf");
    titleStateCoText.setString("Laifu - Test Bed version");
    titleStateCoText.setCharacterSize(150);
    titleStateCoText.setStyle(sf::Text::Bold);
    titleStateCoText.setPosition(200, 150); /* (w) change this static positioning */
}

void TitleState::update(float dt)
{
    fadeTitle();
}

void TitleState::draw(float dt)
{
    /* (w) code will be added later */
    this->stMgr->mWindow.clear(sf::Color::Black);
    this->stMgr->mWindow.draw(titleStateSprite);
    this->stMgr->mWindow.draw(titleStateCoText);
    this->stMgr->mWindow.display();
    if(loadMenu)
        //this->stMgr->pushState(new MenuState(*(this->stMgr)));
        this->stMgr->assignState(new MenuState(*(this->stMgr)));
}

void TitleState::handleinput(sf::Event::KeyEvent &kEvent, bool isPressed)
{
    /* (w) code will be added later */
    if(isPressed)
    {
        switch(kEvent.code)
        {
            case sf::Keyboard::Return:
                if(!this->loadMenu)
                    loadMenu = true;
                break;

            default:
                break;
        }
    }
}

void TitleState::loadTexture()
{
    /* (w) checking for the member variable */
    if(this->titleStateTexture.loadFromFile("./res/menuStateRes/test.png"))
        LOG<<INFO<<"Background Image loaded\n";
    else
        LOG<<ERROR<<"Background Image could not be loaded\n";
    this->titleStateTexture.setSmooth(true);
    this->titleStateSprite.setTexture(this->titleStateTexture);
}

void TitleState::loadFont(const string &inpFontFile)
{
    /* (w) code will be added later */
    if(!this->titleStateCoFont.loadFromFile(inpFontFile))
        LOG<<ERROR<<inpFontFile<<" could not be loaded\n";
    else
        LOG<<INFO<<inpFontFile<<" font loaded\n";

    this->titleStateCoText.setFont(this->titleStateCoFont);
}

void TitleState::fadeTitle()
{
    if(inc)
    {
        titleStateSprite.setColor(sf::Color(titleStateSprite.getColor().r, titleStateSprite.getColor().g, titleStateSprite.getColor().b, alphaVal));
        titleStateCoText.setColor(sf::Color(0, 0, 0, ++alphaVal));
        if(alphaVal == 250)
        {
            dec = true;
            inc = false;
        }
    }
    if(dec)
    {
        titleStateSprite.setColor(sf::Color(titleStateSprite.getColor().r, titleStateSprite.getColor().g, titleStateSprite.getColor().b, alphaVal));
        titleStateCoText.setColor(sf::Color(0, 0, 0, --alphaVal));
        if(alphaVal == 128)
        {
            dec = false;
            /* (w) setting the flag to load the next state */
            loadMenu = true;
        }
    }

    sf::Event titleStateEvt;
    stMgr->mWindow.setKeyRepeatEnabled(false);

    while(stMgr->mWindow.pollEvent(titleStateEvt))
    {
        switch(titleStateEvt.type)
        {
            case sf::Event::LostFocus:
                LOG<<WARNING<<"Game has lost focus\n";
                break;

            case sf::Event::GainedFocus:
                LOG<<INFO<<"Game has gained focus\n";
                break;

            case sf::Event::Closed:
                LOG<<INFO<<"Exiting game demo\n";
                stMgr->mWindow.close();
                break;

            case sf::Event::KeyPressed:
                handleinput(titleStateEvt.key, true);
                break;

            case sf::Event::KeyReleased:
                handleinput(titleStateEvt.key, false);
                break;

            default:
                break;
        }
    }
}

int TitleState::getIdent()
{
    return identity;
}
