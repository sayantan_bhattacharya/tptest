/*
 * =====================================================================================
 *
 *       Filename:  textureBank.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Friday 30 January 2015 01:19:51  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/textureBank.h"

using namespace std;

TextureBank::TextureBank()
{
    /* (w) Default constructor - code will be added later */
}

TextureBank::~TextureBank()
{
    LOG<<INFO<<"TextureBank Destructor called\n";
    for(auto &i : textureStore)
        delete i.second;
}

sf::Texture &TextureBank::getTexture(const string &ID)
{
    if(textureStore.empty())
    {
        LOG<<ERROR<<"No Texture was loaded in the texture bank\n";
        throw EE("No Texture was loaded in the textureBank");
    }

    return *textureStore.at(ID);
}

void TextureBank::setTextureFromFile(const string &ID, const string &filename)
{
    sf::Texture *iTexture = new sf::Texture;

    if(!(iTexture->loadFromFile(filename)))
        throw EE(string("Unable to load file from : " + filename).c_str());

    textureStore[ID] = iTexture;
}
