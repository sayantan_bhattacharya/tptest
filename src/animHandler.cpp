/*
 * =====================================================================================
 *
 *       Filename:  animHandler.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Wednesday 03 June 2015 03:23:57  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/animHandler.h"

AnimHandler::AnimHandler()
{
    numAnim = 0;
    mUnitprite = nullptr;
    mAnim = nullptr;
    animStopped = true;
    manFrameSet = false;
}

AnimHandler::AnimHandler(sf::Sprite *sp, AnimDb inpAnimDb)
{
    mUnitprite = sp;
    mAnim = inpAnimDb;

    numAnim = 0;
    animStopped = true;
    manFrameSet = false;
}

AnimHandler::~AnimHandler()
{
    /* (w) Default destructor - code will be added later */
}

/* (w) rest of the functions go here */

void AnimHandler::setTargetSprite(sf::Sprite *inpSprite)
{
    mUnitprite = inpSprite;
}

void AnimHandler::setAnimList(AnimDb inpAnimDb)
{
    mAnim = inpAnimDb;
}

void AnimHandler::update(float dt)
{
    if(mAnim->empty())
        throw EE("Animation database is empty");

    if(animStopped)
    {
        if(curAnim == "")
            throw EE("No animation was set");
        else if(mUnitprite == nullptr)
            throw EE("Sprite target is not set");
        else if(mAnim == nullptr)
            throw EE("No Animation database was loaded");
        else
        {
            manFrameSet = false;
            Animation &cAnim = mAnim->at(curAnim);
            sf::IntRect sprRect = cAnim.getCurFrame();
            mUnitprite->setTextureRect(sprRect);
            cAnim.updateFrame(dt);
        }
    }
    else
    {
        Animation &cAnim = mAnim->at(curAnim);
        sf::IntRect sprRect;
        if(!manFrameSet)
            sprRect = cAnim.getFrame(0);
        else
            sprRect = cAnim.getCurFrame();
        mUnitprite->setTextureRect(sprRect);
    }
}

void AnimHandler::setAnimFrame(size_t inpFrame)
{
    manFrameSet = true;
    Animation &cAnim = mAnim->at(curAnim);
    sf::IntRect sprRect = cAnim.getFrame(inpFrame);
    mUnitprite->setTextureRect(sprRect);
}

void AnimHandler::startAnim()
{
    animStopped = true;
}

void AnimHandler::stopAnim()
{
    animStopped = false;
}

void AnimHandler::setCurAnim(const std::string &animOffset)
{
    curAnim = animOffset;
}
