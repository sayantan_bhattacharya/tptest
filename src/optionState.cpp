/*
 * =====================================================================================
 *
 *       Filename:  optionState.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Sunday 29 March 2015 10:58:31  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/optionState.h"

int OptionState::stateChoice = 0;

OptionState::OptionState(StateManager &stateMgr)
{
    stMgr = &stateMgr;
    identity = OPTIONSTATE;
    LOG<<INFO<<"Option state constructor called\n";

    /* (w) load the textures for this state */
    loadTexture();
    //stMgr->gmConfig.checkConfig();
    //stMgr->gmConfig.populateVector(GSETTINGSW); /* (w) Fixing the fileWrite() function first */
    //stMgr->gmConfig.populateVector(GSETTINGSR); /* (w) Have to check the file read now, the fileWrite seems to be working fine */
    loadFont("./res/menuStateRes/testFont.ttf");
    loadOptionsText();

    /* (w) check the modes available for the game */
    //stMgr->gmConfig.fullScrVidModes(); /* (w) Removed the duplicate values in the video modes - now to return the vector os video modes */
}

OptionState::~OptionState()
{
    /* (w) code will be added later */
}

void OptionState::draw(float dt)
{
    /* (w) removing the screen clear color */
    stMgr->mWindow.clear(sf::Color::Black);
    stMgr->mWindow.draw(optionStateBgSprite);
    displayOptions(); /* (w) writing the body of this function */
    stMgr->mWindow.display();
}

void OptionState::update(float dt)
{
    /* (w) code will be added later */
    sf::Event optionStateEvt;
    stMgr->mWindow.setKeyRepeatEnabled(false);

    while(stMgr->mWindow.pollEvent(optionStateEvt))
    {
        switch(optionStateEvt.type)
        {
            case sf::Event::LostFocus:
                LOG<<WARNING<<"Game has lost focus\n";
                break;

            case sf::Event::GainedFocus:
                LOG<<INFO<<"Game has gained focus\n";
                break;

            case sf::Event::Closed:
                LOG<<INFO<<"Exiting game demo\n";
                stMgr->mWindow.close();
                break;

            case sf::Event::KeyPressed:
                handleinput(optionStateEvt.key, true);
                break;

            case sf::Event::KeyReleased:
                handleinput(optionStateEvt.key, false);
                break;

            default:
                break;
        }
    }
}

void OptionState::handleinput(sf::Event::KeyEvent &kEvent, bool isPressed)
{
    if(isPressed)
    {
        switch(kEvent.code)
        {
            case sf::Keyboard::Escape:
                /* (w) before moving to the menuState - write the config file again */
                stMgr->gmConfig.populateVector(GSETTINGSW);

                /* (w) Move to the menu state */
                LOG<<INFO<<"Going back to the menustate\n";
                stMgr->assignState(new MenuState(*stMgr));
                break;

            case sf::Keyboard::Up:
                if(stateChoice == (int)CONFIGOPTIONS::RESOLUTION)
                    stateChoice = (int)CONFIGOPTIONS::SOUNDFXVOL;
                else
                    --stateChoice;
                break;

            case sf::Keyboard::Down:
                if(stateChoice == (int)CONFIGOPTIONS::SOUNDFXVOL)
                    stateChoice = (int)CONFIGOPTIONS::RESOLUTION;
                else
                    ++stateChoice;
                break;

            case sf::Keyboard::Left:
                cycleOptionVal(true); /* (w) For the left key, the value will be true */
                break;

            case sf::Keyboard::Right:
                cycleOptionVal(false); /* (w) for right keym the value needs to be decremented, hence sending false */
                break;

            default:
                break;
        }
    }
}

int OptionState::getIdent()
{
    return identity;
}

void OptionState::loadTexture()
{
    /* (w) load the optionState background
     * Keeping the same background color for the option state */
    optionStateBg.loadFromFile("./res/menuStateRes/test.png"); /* (w) Move the same picture to another directory */
    optionStateBg.setSmooth(true);
    optionStateBgSprite.setTexture(optionStateBg);
    optionStateBgSprite.setColor(sf::Color(255, 255, 255, 128));
}

void OptionState::loadFont(const string& fontFilePath)
{
    if(!optionStateFont.loadFromFile(fontFilePath))
        LOG<<ERROR<<fontFilePath<<" : File could not be found\n";
    else
        LOG<<INFO<<fontFilePath<<" : File loaded\n";

    optionStateText.setFont(optionStateFont);
}

/* (w) Private functions written below */

void OptionState::loadOptionsText()
{
    optStateDispData.push_back(make_pair((int)CONFIGOPTIONS::RESOLUTION, "Resolution"));
    optStateDispData.push_back(make_pair((int)CONFIGOPTIONS::FULLSCREEN, "Fullscreen"));
    optStateDispData.push_back(make_pair((int)CONFIGOPTIONS::VIDEOSYNC, "VSync"));
    optStateDispData.push_back(make_pair((int)CONFIGOPTIONS::MUSICVOL, "Music Volume"));
    optStateDispData.push_back(make_pair((int)CONFIGOPTIONS::SOUNDFXVOL, "SFX Volume"));
}

void OptionState::displayOptions()
{
    /* (w) Need to add a switch here based on the first value from the vector */
    for(auto &iter : optStateDispData)
    {
        if(iter.first == stateChoice)
        {
            string tempStr;
            tempStr.clear();
            tempStr = iter.second;
            switch(iter.first)
            {
                case (int)CONFIGOPTIONS::RESOLUTION:
                    tempStr.append("\t" + to_string(stMgr->gmConfig.getWinResolution().first) + " X " + to_string(stMgr->gmConfig.getWinResolution().second));
                    break;

                case (int)CONFIGOPTIONS::FULLSCREEN:
                    if(stMgr->gmConfig.getFullScr())
                        tempStr.append("\tTrue");
                    else
                        tempStr.append("\tFalse");
                    break;

                case (int)CONFIGOPTIONS::VIDEOSYNC:
                    if(stMgr->gmConfig.getVSync())
                        tempStr.append("\tTrue");
                    else
                        tempStr.append("\tFalse");
                    break;

                case (int)CONFIGOPTIONS::MUSICVOL:
                    tempStr.append("\t" + to_string(stMgr->gmConfig.getMusVol()));
                    break;

                case (int)CONFIGOPTIONS::SOUNDFXVOL:
                    tempStr.append("\t" + to_string(stMgr->gmConfig.getSfxVol()));
                    break;

                default:
                    LOG<<WARNING<<"Unknown value provided\n";
                    break;
            }
            optionStateText.setString("[" + tempStr + "]");
        }
        else
            optionStateText.setString(iter.second);

        optionStateText.setColor(sf::Color::Black); /* (w) Change the values that are not default, make them dynamic by calculating the value */
        optionStateText.setCharacterSize(DEFAULT_CHARACTER_SIZE);
        optionStateText.setStyle(sf::Text::Bold);
        optionStateText.setPosition(DEFAULT_TEXTPOS, DEFAULT_TEXTPOS + iter.first * DEFAULT_TEXT_SEP);

        /* (w) display the options text */
        stMgr->mWindow.draw(optionStateText);
    }
    optionStateText.setString("[Esc] Back\t[Up / Down] Select\t[Left / Right] Change");
    optionStateText.setColor(sf::Color::Black);
    optionStateText.setCharacterSize(DEFAULT_CHARACTER_SIZE);
    optionStateText.setStyle(sf::Text::Bold);
    optionStateText.setPosition(DEFAULT_TEXTPOS, 250.f); /* (w) make this magic number dynamic - calculate it over here */

    /* (w) display this one too */
    stMgr->mWindow.draw(optionStateText);
}

void OptionState::cycleOptionVal(bool choice)
{
    std::pair<unsigned int, unsigned int> tempPair;
    switch(stateChoice)
    {
        case (int)CONFIGOPTIONS::RESOLUTION:
            /* (w) Call the function in the game config class to circulate and set the values */
            tempPair = stMgr->gmConfig.circResolution(stMgr->gmConfig.getWinResolution().first, stMgr->gmConfig.getWinResolution().second, choice);
            stMgr->gmConfig.setWinResolution(tempPair.first, tempPair.second);
            break;

        case (int)CONFIGOPTIONS::FULLSCREEN:
            if(stMgr->gmConfig.getFullScr())
                stMgr->gmConfig.setFullScr(0);
            else
                stMgr->gmConfig.setFullScr(1);
            break;

        case (int)CONFIGOPTIONS::VIDEOSYNC:
            if(stMgr->gmConfig.getVSync())
                stMgr->gmConfig.setVSync(0);
            else
                stMgr->gmConfig.setVSync(1);
            break;

        case (int)CONFIGOPTIONS::MUSICVOL: /* (w) write the routine for this */
            if(choice && (stMgr->gmConfig.getMusVol() < 1.0)) /* (w) Maximum value for sound is 1.0 */
                stMgr->gmConfig.setMusVol(stMgr->gmConfig.getMusVol() + STEP);
            if(!choice && (stMgr->gmConfig.getMusVol() > 0.0000001)) /* (w) this portion of the checking seems hinky to me */
                stMgr->gmConfig.setMusVol(stMgr->gmConfig.getMusVol() - STEP);
            break;

        case (int)CONFIGOPTIONS::SOUNDFXVOL: /* (w) write the routine for this */
            if(choice && (stMgr->gmConfig.getSfxVol() < 1.0))
                stMgr->gmConfig.setSfxVol(stMgr->gmConfig.getSfxVol() + STEP);
            if(!choice && (stMgr->gmConfig.getSfxVol() > 0.0000001)) /* (w) this portion of checking seems hinky to me */
                stMgr->gmConfig.setSfxVol(stMgr->gmConfig.getSfxVol() - STEP);
            break;

        default:
            LOG<<INFO<<"Unexpected choice provided\n";
            break;
    }
}
