/*
 * =====================================================================================
 *
 *       Filename:  fileIO.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Wednesday 15 April 2015 10:27:48  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck(),
 *   Organization:
 *        Website: http://www.adp-gmbh.ch/cpp/common/base64.html for base64Encoder and b64Decoder code help
 *
 * =====================================================================================
 */

#include "../inc/fileIO.h"

using namespace std;

/* (w) Hope the base64 encoding and decoding funtion works - because certain names of variables have been changed */
static const std::string b64Chars =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";

static inline bool isB64(unsigned char ch)
{
    return (isalnum(ch) || (ch == '+') || (ch == '/'));
}

string FileIO::b64Encode(unsigned char const *byteEncode, unsigned int len)
{
    std::string ret;
    int i = 0;
    int j = 0;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];

    while(len--)
    {
        char_array_3[i++] = *(byteEncode++);
        if (i == 3)
        {
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;

            for(i = 0; (i <4) ; i++)
                ret += b64Chars[char_array_4[i]];
            i = 0;
        }
    }

    if (i)
    {
        for(j = i; j < 3; j++)
            char_array_3[j] = '\0';

        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        char_array_4[3] = char_array_3[2] & 0x3f;

        for (j = 0; (j < i + 1); j++)
            ret += b64Chars[char_array_4[j]];

        while((i++ < 3))
            ret += '=';

      }
      return ret;
}

string FileIO::b64Decode(const string &str)
{
    int in_len = str.size();
    int i = 0;
    int j = 0;
    int in_ = 0;
    unsigned char char_array_4[4], char_array_3[3];
    std::string ret;

    while (in_len-- && ( str[in_] != '=') && isB64(str[in_]))
    {
        char_array_4[i++] = str[in_]; in_++;
        if (i ==4)
        {
            for (i = 0; i < 4; i++)
                char_array_4[i] = b64Chars.find(char_array_4[i]);

            char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
            char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
            char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

            for (i = 0; i < 3; i++)
                ret += char_array_3[i];
            i = 0;
        }
    }

    if (i)
    {
        for (j = i; j <4; j++)
            char_array_4[j] = 0;

        for (j = 0; j <4; j++)
            char_array_4[j] = b64Chars.find(char_array_4[j]);

        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
        char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

        for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
    }

    return ret;
}

FileIO::FileIO()
{
    /* (w) default constructor - code will be added later */
}

FileIO::~FileIO()
{
    /* (w) Default destructor - code will be added later */
}

bool FileIO::fileExists(const string &inpConfigFile)
{
    /* (w) code will be added later */
    struct stat buffer;
    return (stat(inpConfigFile.c_str(), &buffer) == 0);
}

void FileIO::fileWrite(const string fileName, vector<pair<int, double>> &inpConfVec)
{
    /* (w) Now start writing the file using this function and the fileName that is being provided */
    ofstream outFile(fileName, ios::out | ios::binary);
    //LOG<<INFO<<"Configuration Vector size : "<<inpConfVec.size()<<endl;
    for(auto &iter : inpConfVec)
    {
        /* (w) encrypt the file contents before writing the same in the file */
        string writeVal = b64Encode(reinterpret_cast<const unsigned char *>(to_string(iter.second).c_str()), to_string(iter.second).length());
        outFile.write(writeVal.c_str(), sizeof(string));
    }
    outFile.close();
}

void FileIO::fileRead(const string fileName, vector<pair<int, double>> &inpConfVec)
{
    /* (w) the files that are going to be read are as follows:
     * 1. Game Configuration file
     * 2. Profile file
     * 3. Inventory file
     * All these files will be binary files and will have data written in them in the following format
     * VALUE in base64 encrypted format
     *
     * Game Configuration file
     * Exactly in the same format the macros are defined in the globals header*/

    ifstream inFile(fileName.c_str(), ios::in | ios::binary);

    /* (w) start reading the file, after reading each encrypted string, decrypt the same and push the same in the configVec that will be passed to this function */
    uint32_t count = sizeof(string);
    string readVal(count, ' ');
    double iVal = 0;
    auto iter = 0;
    while(inFile.read(&readVal[0], count))
    {
        iVal = stod(b64Decode(readVal));
        /* (w) make the pair again */
        inpConfVec[iter] = make_pair(inpConfVec[iter].first, iVal);
        ++iter;
    }

    inFile.close();
}
