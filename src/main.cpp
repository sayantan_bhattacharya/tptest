/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Saturday 20 December 2014 07:31:32  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/main.h"

using namespace std;

int main(int argc, char *argv[])
{
    try
    {
        StateManager gameStateMgr;
        gameStateMgr.assignState(new TitleState(gameStateMgr));
        gameStateMgr.gameLoop();
    }
    catch(ExitException &exitExc)
    {
        LOG<<ERROR<<exitExc.what()<<endl;
        return 1;
    }
    catch(exception &exc)
    {
        LOG<<ERROR<<exc.what()<<endl;
        return 2;
    }
    catch(...)
    {
        LOG<<ERROR<<"FATAL : Unhandled Exception\n";
        return 3;
    }
    return 0;   /* (w) Hoping that the program I am writing this time will complete properly */
}
