/*
 * =====================================================================================
 *
 *       Filename:  playerUnit.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Saturday 31 January 2015 05:58:25  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/playerUnit.h"

PlayerUnit::PlayerUnit()
{
    /* (w) code will be added later */
    mSprite.setPosition((float)mPosition.x, (float)mPosition.y);
    LOG<<INFO<<"Setting up the animation handler]\n";
    mAnimHandler.setTargetSprite(&mSprite);
    mAnimHandler.setAnimList(&animDatabase);
    mState = UnitState::FACE_R; /* (w) default facing after any load would be right only */

    /* (w) other values */
    puHealth = 100.f;
    puAccel = 1.3f;
    puSpeed = 0.6f;

    /* (w) most probably the ID is for the collision handle case, not including at the moment */
    mAnimHandler.stopAnim();
}

PlayerUnit::~PlayerUnit()
{
    /* (w) code will be added later */
}

void PlayerUnit::handleinput(sf::Event::KeyEvent &kEvent, bool isPressed)
{
    if(isPressed)
    {
        /* (w) code will be added later */
    }
    else
    {
        switch(kEvent.code)
        {
            case sf::Keyboard::D:
                LOG<<INFO<<"D button was pressed Go Right ->\n";
                /* (w) appropriate code for the right movement will be added later */
                mAnimHandler.setCurAnim("walkRightAnim");
                mAnimHandler.stopAnim();
                mVel.x = 0;
                mState = UnitState::FACE_R;
                break;

            case sf::Keyboard::A:
                LOG<<INFO<<"A button was pressed Go Left <-\n";
                /* (w) appropriate code for the left movement will be added later */
                mAnimHandler.setCurAnim("walkLeftAnim");
                mAnimHandler.stopAnim();
                mVel.x = 0;
                mState = UnitState::FACE_L;
                break;

            case sf::Keyboard::W:
                LOG<<INFO<<"W button was pressed Jump now ^\n";
                /* (w) appropriate code for the upwards movement will be added later */
                break;

            default:
                break;
        }
    }
}

void PlayerUnit::handleKeysRealTime(float dt)
{
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        mAnimHandler.setCurAnim("walkRightAnim");
        mAnimHandler.startAnim();
        mVel.x = puSpeed;
        hasMoved = true;
        mState = UnitState::WALK_R;
    }
    else if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        mAnimHandler.setCurAnim("walkLeftAnim");
        mAnimHandler.startAnim();
        mVel.x = -puSpeed;
        hasMoved = true;
        mState = UnitState::WALK_L;
    }
}

void PlayerUnit::update(float dt)
{
    /* (w) Animate the unit */
    mAnimHandler.update(dt);

    /* (w) Move the player unit according to the current velocity */
    mSprite.move(mVel);
    mPosition.x = static_cast<int>(mSprite.getPosition().x);
    mPosition.y = static_cast<int>(mSprite.getPosition().y);

    /* (w) Gravity will be applied later based on the game design */
}

void PlayerUnit::draw(sf::RenderTarget &inpTarget, sf::RenderStates states) const
{
    inpTarget.draw(mSprite, states);
}

void PlayerUnit::loadSpriteSheet(const sf::Texture &inpTexture)
{
    mSpriteSheet = &inpTexture;
    mSprite.setTexture(inpTexture, true);
    generateAnimations();
}

void PlayerUnit::generateAnimations()
{
    /* (w) Animation for moving right */
    animDatabase["walkRightAnim"].addFrame(sf::IntRect(36, 4, 24, 28));
    animDatabase["walkRightAnim"].addFrame(sf::IntRect(68, 4, 24, 28));
    animDatabase["walkRightAnim"].addFrame(sf::IntRect(100, 4, 24, 28));
    animDatabase["walkRightAnim"].setDuration(0.4f);
    animDatabase["walkRightAnim"].setLooping(true);

    /* (w) Animation for moving left */
    animDatabase["walkLeftAnim"].addFrame(sf::IntRect(132, 4, 24, 28));
    animDatabase["walkLeftAnim"].addFrame(sf::IntRect(164, 4, 24, 28));
    animDatabase["walkLeftAnim"].addFrame(sf::IntRect(196, 4, 24, 28));
    animDatabase["walkLeftAnim"].setDuration(0.4f);
    animDatabase["walkLeftAnim"].setLooping(true);

    /* (w) set the default animation */
    mAnimHandler.setCurAnim("walkRightAnim");
}
