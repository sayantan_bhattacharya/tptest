/*
 * =====================================================================================
 *
 *       Filename:  gameProfile.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Wednesday 23 September 2015 02:05:22  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/gameProfile.h"

/* (w) check if the profile file gets created or not when newGame is called */
GameProfile::GameProfile()
{
    if(fileHandle.fileExists(PROFILE_FILE))
    {
        LOG<<INFO<<"Profile file/game save file found\n";
        profileExists = true;
    }
    else
    {
        /* (w) code will be added shortly */
        LOG<<WARNING<<"Could not find game save/profile file, creating new one : "<<PROFILE_FILE<<std::endl;
        profileExists = false;
    }
    pHealth = MAX_HEALTH;
    mPlayerLevel = GLEVEL::LEVELA;
    mPlayerLocX = DEFAULT_PLAYER_LOCX;
    mPlayerLocY = DEFAULT_PLAYER_LOCY;
}

void GameProfile::checkProfile(bool resetSave)
{
    /* (w) code for the same will be added shortly */
    if(profileExists)
    {
        LOG<<INFO<<"Reading the profile file\n";
        if(resetSave)
        {
            pHealth = MAX_HEALTH;
            mPlayerLevel = GLEVEL::LEVELA;
            mPlayerLocX = DEFAULT_PLAYER_LOCX;
            mPlayerLocY = DEFAULT_PLAYER_LOCY;
            populateVector(PROFILEW);
        }
        populateVector(PROFILER);
    }
    else
    {
        LOG<<WARNING<<"Creating new profile file - seems like a new game is being started\n";
        pHealth = MAX_HEALTH;
        mPlayerLevel = GLEVEL::LEVELA;
        mPlayerLocX = DEFAULT_PLAYER_LOCX;
        mPlayerLocY = DEFAULT_PLAYER_LOCY;

        /* (w) Now write the file in the same location as that of the configuration */
        populateVector(PROFILEW);
    }
}

GameProfile::~GameProfile()
{
    /* (w) Default destructor, code will be added later */
}

void GameProfile::populateVector(int mode)
{
    int offset = 0;
    switch(mode)
    {
        case (int)MODES::PROFILER:
            /* (w) read the profile file and load the values in the vector */
            offset = (int)PROFILE::HEALTH;
            configVec.clear();

            /* (w) set up the first portions of the vector pair */
            for(auto iter = 0; iter < (int)PROFILE::PROFMEMCOUNT; ++iter)
                configVec.push_back(make_pair(offset + iter, 0));

            //for(auto index = 0; index < (int)PROFILE::PROFMEMCOUNT; ++index)
                //LOG<<INFO<<"Before reading the file : "<<configVec[index].first<<" "<<configVec[index].second<<std::endl;

            /* (w) Read the file now */
            fileHandle.fileRead(PROFILE_FILE, configVec);

            /* (w) NOTE : Do not call any function of this class, the profile file is not present */
            /* (w) Now try to see if the values are being read correctly */
            //for(auto index = 0; index < (int)PROFILE::PROFMEMCOUNT; ++index)
                //LOG<<INFO<<"After reading the file : "<<configVec[index].first<<" "<<configVec[index].second<<std::endl;
            parseVec(); /* (w) profile values are read properly, then why is the sprite saved at that point */
            break;

        case (int)MODES::PROFILEW:
            offset = (int)PROFILE::HEALTH;
            configVec.clear();

            /* (w) populate the identifiers in the vector before writing the file */
            for(auto iter = 0; iter < (int)PROFILE::PROFMEMCOUNT; ++iter)
                configVec.push_back(make_pair(offset + iter, 0));
            /* (w) code for writing the contents of the profile will be added shortly */
            setValVec();
            fileHandle.fileWrite(PROFILE_FILE, configVec);
            break;

        default:
            break;
    }
}

void GameProfile::setHealth(long inpHealth)
{
    pHealth = inpHealth;
}

long GameProfile::getHealth()
{
    return pHealth;
}

void GameProfile::setLocation(std::pair<int, int> inpLocPair)
{
    mPlayerLocX = inpLocPair.first;
    mPlayerLocY = inpLocPair.second;
}

void GameProfile::setLocation(int inpLocX, int inpLocY)
{
    mPlayerLocX = inpLocX;
    mPlayerLocY = inpLocY;
}

std::pair<int, int> GameProfile::getLocation()
{
    return std::make_pair(mPlayerLocX, mPlayerLocY);
}

void GameProfile::setLevel(int inpLevel)
{
    mPlayerLevel = inpLevel;
}

int GameProfile::getLevel()
{
    return mPlayerLevel;
}

void GameProfile::setLocationX(int inpLocX)
{
    mPlayerLocX = inpLocX;
}

void GameProfile::setLocationY(int inpLocY)
{
    mPlayerLocY = inpLocY;
}

void GameProfile::setValVec()
{
    /* (w) code will be added shortly */
    for(auto iter = 0; iter < (int)PROFILE::HEALTH; ++iter)
    {
        switch(configVec[iter].first)
        {
            case (int)PROFILE::HEALTH:
                configVec[iter] = std::make_pair(configVec[iter].first, pHealth);
                break;

            case (int)PROFILE::PLAYERLOCX:
                configVec[iter] = std::make_pair(configVec[iter].first, mPlayerLocX);
                break;

            case (int)PROFILE::PLAYERLOCY:
                configVec[iter] = std::make_pair(configVec[iter].first, mPlayerLocY);
                break;

            case (int)PROFILE::PLAYERLEVEL:
                configVec[iter] = std::make_pair(configVec[iter].first, mPlayerLevel);
                break;

            default:
                break;
        }
    }
}

void GameProfile::parseVec()
{
    for(auto &iElem : configVec)
    {
        switch(iElem.first)
        {
            case (int)PROFILE::HEALTH:
                setHealth(iElem.second);
                break;

            case (int)PROFILE::PLAYERLOCX:
                setLocationX(iElem.second);
                break;

            case (int)PROFILE::PLAYERLOCY:
                setLocationY(iElem.second);
                break;

            case (int)PROFILE::PLAYERLEVEL:
                setLevel(iElem.second);
                break;

            default:
                break;
        }
    }
}
