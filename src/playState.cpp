/*
 * =====================================================================================
 *
 *       Filename:  playState.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Tuesday 23 December 2014 01:21:52  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/playState.h"

using namespace std;

PlayState::PlayState(StateManager &stateMgr):world(stateMgr)
{
    stMgr = &stateMgr;
    identity = PLAYSTATE;
    LOG<<INFO<<"PlayState created\n";
    loadTexture(); /* (w) need to write the function body - this has been missed out */
}

PlayState::~PlayState()
{
    /* (w) code will be added later */
}

void PlayState::draw(float dt) /* (w) check for the code change that needs to be done here */
{
    stMgr->mWindow.clear(sf::Color::Black);
    world.draw(dt);
    stMgr->mWindow.display();
}

void PlayState::update(float dt)
{
    /*
     * TODO: On Press of sf::Keyboard::Escape - change the state to menuState and vice-versa
     */

    sf::Event playStateEvt;
    stMgr->mWindow.setKeyRepeatEnabled(false);  /* (w) Pressing down a key won't work */

    while(stMgr->mWindow.pollEvent(playStateEvt))
    {
        switch(playStateEvt.type)
        {
            case sf::Event::LostFocus:
                stMgr->hasLostFocus = true;
                LOG<<WARNING<<"Game has lost focus\n";
                break;

            case sf::Event::GainedFocus:
                stMgr->hasLostFocus = false;
                LOG<<INFO<<"Game has gained focus\n";
                break;

            case sf::Event::Closed:
                LOG<<INFO<<"Exiting game demo\n";
                stMgr->mWindow.close();
                break;

            case sf::Event::KeyPressed:
                world.handleinput(playStateEvt.key, true);
                handleinput(playStateEvt.key, true);
                break;

            case sf::Event::KeyReleased:
                world.handleinput(playStateEvt.key, false);
                handleinput(playStateEvt.key, false);
                break;

            default:
                break;
        }
    }
    world.handleKeysRealTime(dt);
    world.update(dt);
}

void PlayState::handleinput(sf::Event::KeyEvent &kEvent, bool isPressed)
{
    /* (w) Handle events for PlayState only */
    if(isPressed)
    {
        switch(kEvent.code)
        {
            case sf::Keyboard::Escape:
                LOG<<INFO<<"Going back to the MenuState\n";
                stMgr->assignState(new MenuState(*stMgr));
                break;

            default:
                break;
        }
    }
}

void PlayState::loadTexture()
{
    /* (w) decision pending on how to load the textures */
    world.loadTexture();
}

int PlayState::getIdent()
{
    return identity;
}
