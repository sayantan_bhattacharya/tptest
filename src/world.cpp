/*
 * =====================================================================================
 *
 *       Filename:  world.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Saturday 24 January 2015 02:22:40  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/world.h"

World::World(StateManager &stateMgr):tileDim(32)
{
    stMgr = &stateMgr;
    LOG<<INFO<<"Player World created\n";

    LOG<<INFO<<"Reading game save file\n";
    stMgr->gmProfile.checkProfile();
    stMgr->gmProfile.populateVector(PROFILER);

    /* (w) Let's add the other stuff now */
    puMain.setNodePosition(stMgr->gmProfile.getLocation().first, stMgr->gmProfile.getLocation().second);
    puMain.setNodeDimensions(tileDim, tileDim);

    scGraph.addSceneNode(0, "player", puMain);
}

World::~World()
{
    /* (w) code will be added later */
}

void World::handleinput(sf::Event::KeyEvent &kEvent, bool isPressed)
{
    /* (w) call the playerUnit handleInput function */
    puMain.handleinput(kEvent, isPressed);
}

void World::handleKeysRealTime(float dt)
{
    /* (w) Have to call the player unit to handle the real time key press */
    if(!stMgr->hasLostFocus)   /* (w) call the handleKeysRealTime function of the PlayerUnit */
        puMain.handleKeysRealTime(dt);
}

void World::update(float dt)
{
    for(size_t iter = 0; iter < (unsigned long)scGraph.numOfLayers(); iter++) /* (w) check for the sceneGraph object being created */
    {
        for(auto &iNode : scGraph.returnLayer(iter))
        {
            iNode.second->update(dt);
            if(iNode.second->hasMoved)
            {
                iNode.second->hasMoved = false; /* (w) Was it only to update the collision graph - might have to check because of Box2D integration now */
                if(iNode.second->getPosition().first == 200)
                {
                    /* (w) set the values before calling the profile write game save function */
                    stMgr->gmProfile.setLocation(iNode.second->getPosition().first, iNode.second->getPosition().second);
                    stMgr->gmProfile.populateVector(PROFILEW);
                }
            }
        }
    }
}

void World::loadTexture()
{
    LOG<<INFO<<"Loading the textures for the world\n";
    /* (w) first check the level for which data needs to be loaded */
    worldTextureBankInst.setTextureFromFile("player", "./res/playStateRes/playerRes/walkAnim.png");

    LOG<<INFO<<"Creating animations\n";
    puMain.loadSpriteSheet(worldTextureBankInst.getTexture("player"));
}

void World::draw(float dt)
{
    for(long index = 0; index < scGraph.numOfLayers(); index++)
        for(auto &iNode : scGraph.returnLayer(index))
            stMgr->mWindow.draw(*iNode.second);
}
