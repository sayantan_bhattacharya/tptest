/*
 * =====================================================================================
 *
 *       Filename:  menuState.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Monday 22 December 2014 03:04:08  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/menuState.h"

using namespace std;

/*
 * The following options will be available in the menu
 * continue(greyed out if no save data found)
 * New Game (always enabled)
 * Options (settings state)
 * Credits (credit screen state)
 * Quit (Exit the game)
 */

int MenuState::stateChoice = 0;

MenuState::MenuState(StateManager &stateMgr)
{
    /* (w) Basic code will be added later */
    stMgr = &stateMgr;
    identity = MENUSTATE;
    LOG<<INFO<<"Menu state created\n";
    loadTexture();
    loadFont("./res/menuStateRes/testFont.ttf");
    loadMenuOptions();

    if(stMgr->gmProfile.profileExists)
        MenuState::stateChoice = 0;
    else
        MenuState::stateChoice = 1;
}

void MenuState::draw(float dt)
{
    stMgr->mWindow.clear(sf::Color::Black);
    stMgr->mWindow.draw(menuStateBgSprite);
    displayMenuOptions();
    stMgr->mWindow.display();
}

void MenuState::update(float dt)
{
    sf::Event menuStateEvt;
    stMgr->mWindow.setKeyRepeatEnabled(false);

    while(stMgr->mWindow.pollEvent(menuStateEvt))
    {
        switch(menuStateEvt.type)
        {
            case sf::Event::LostFocus:
                LOG<<WARNING<<"Game has lost focus\n";
                break;

            case sf::Event::GainedFocus:
                LOG<<INFO<<"Game has gained focus\n";
                break;

            case sf::Event::Closed:
                LOG<<INFO<<"Exiting game demo\n";
                stMgr->mWindow.close();
                break;

            case sf::Event::KeyPressed:
                handleinput(menuStateEvt.key, true);
                break;

            case sf::Event::KeyReleased:
                handleinput(menuStateEvt.key, false);
                break;

            default:
                break;
        }

    }
}

void MenuState::displayMenuOptions()
{
    for(auto iter = menuStateOptions.begin(); iter != menuStateOptions.end(); ++iter)
    {
        if(iter->first == stateChoice)
            menuStateOptText.setString("[" + iter->second + "]");
        else
            menuStateOptText.setString(iter->second);

        menuStateOptText.setColor(sf::Color::Black);
        menuStateOptText.setCharacterSize(DEFAULT_CHARACTER_SIZE);
        menuStateOptText.setStyle(sf::Text::Bold);
        menuStateOptText.setPosition(DEFAULT_TEXTPOS, DEFAULT_TEXTPOS + iter->first * DEFAULT_TEXT_SEP);

        /* (w) display the text */
        stMgr->mWindow.draw(menuStateOptText);
    }
}

void MenuState::handleinput(sf::Event::KeyEvent &kEvent, bool isPressed)
{
    /* (w) Handle all key inputs for current state and not game elements */
    if(isPressed)
    {
        switch(kEvent.code)
        {
            case sf::Keyboard::Return:
                if(stateChoice == QUIT)
                    stMgr->mWindow.close();
                else if(stateChoice == NEW_GAME) /* (w) remove the existing instance and insert a new one */
                {
                    stMgr->gmProfile.checkProfile(true);/* (w) need to rewrite the existing profile with the default values */
                    stMgr->assignState(new PlayState(*stMgr), true);
                    stMgr->gmProfile.profileExists = true;
                }
                else if(stateChoice == OPTIONS)
                    stMgr->assignState(new OptionState(*stMgr));
                else if(stateChoice == CONTINUE)   /* (w) On Continue search snd use the existing instance */
                {
                    stMgr->gmProfile.checkProfile();
                    stMgr->assignState(new PlayState(*stMgr));
                    stMgr->gmProfile.profileExists = true;
                }
                break;

            case sf::Keyboard::Up:
                if(stateChoice == CONTINUE && stMgr->gmProfile.profileExists)
                    stateChoice = QUIT;
                else if(stateChoice == NEW_GAME && !stMgr->gmProfile.profileExists)
                    stateChoice = QUIT;
                else
                    --stateChoice;
                break;

            case sf::Keyboard::Down:
                if(stateChoice == QUIT)
                    if(stMgr->gmProfile.profileExists)
                        stateChoice = CONTINUE;
                    else
                        stateChoice = NEW_GAME;
                else
                    ++stateChoice;
                break;

            default:
                break;
        }
    }
}

void MenuState::loadMenuOptions()
{
    /* CONTINUE,
     * NEW_GAME,
     * OPTIONS,
     * CREDITS,
     * QUIT
     * */
    menuStateOptions.push_back(make_pair(CONTINUE, "Continue"));
    menuStateOptions.push_back(make_pair(NEW_GAME, "New Game"));
    menuStateOptions.push_back(make_pair(OPTIONS, "Options"));
    menuStateOptions.push_back(make_pair(CREDITS, "Credits"));
    menuStateOptions.push_back(make_pair(QUIT, "Quit"));
}

void MenuState::loadTexture()
{
    /* (w) remove the statically mentioned files that are being loaded */
    menuStateBg.loadFromFile("./res/menuStateRes/test.png");
    menuStateBg.setSmooth(true);
    menuStateBgSprite.setTexture(menuStateBg);
    menuStateBgSprite.setColor(sf::Color(255, 255, 255, 128)); /* (w) remove the magic numbers that are present in here */
}

void MenuState::loadFont(const string &inpFontFile)
{
    if(!menuStateFont.loadFromFile(inpFontFile))
        LOG<<ERROR<<inpFontFile<<" File could not be loaded\n";
    else
        LOG<<INFO<<inpFontFile<<" File loaded\n";

    menuStateOptText.setFont(menuStateFont);
}

int MenuState::getIdent()
{
    return identity;
}
