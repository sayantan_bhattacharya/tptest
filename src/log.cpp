/*
 * =====================================================================================
 *
 *       Filename:  log.cpp
 *
 *    Description:  Implementation of log file
 *
 *        Version:  1.0
 *        Created:  12/06/2014 11:43:54 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Nilangshu Bidyanta (), nbidyanta@gmail.com
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/log.h"

using namespace std;

logDump LOG;

logDump::logDump()
{

}

logDump::logDump(const string &fileName)
{
	fName = fileName;
	setFile(fileName);
}

logDump::~logDump()
{
	if(outFile.is_open())
	{
		/* Close the log file */
		outFile.close();

		/* Strip off the ANSI color codes from the file */
		ifstream ifile;
		ofstream ofile;
		ifile.open(fName, ios::in);
		ofile.open(fName+".log", ios::out);

		/* Read file character by character */
		while(ifile.good())
		{
			uint32_t ch=ifile.get();
			/* ANSI color codes are enclosed
			 * between 0x1B and the letter 'm' */
			if(ch==0x1B)
			{
				while(ch!='m' && ifile.good())
					ch=ifile.get();
				ch=ifile.get();
			}
			if(ch!=EOF)
				ofile<<(char)ch;
		}
		ifile.close();
		ofile.close();
	}
	LOG<<STD;
}

void logDump::setFile(const string &fileName)
{
	fName = fileName;
	outFile.open(fileName, ios::out);
}

logDump &logDump::operator<<(ostream &(*manip)(ostream &))
{
	manip(cout);
	manip(outFile);
	return (*this);
}

ostream &STD(ostream &os)
{
	/* IO Manipulator to mark the stream as standard */
	string nattrib = "\033[0m";
	for(auto ch : nattrib)
		os.put(ch);
	return os;
}

ostream &INFO(ostream &os)
{
	/* IO Manipulator to mark the stream as an information */
	string info = "\033[1;42;37mINFO:\033[0m \033[32m";
	for(auto ch : info)
		os.put(ch);
	return os;
}

ostream &WARNING(ostream &os)
{
	/* IO Manipulator to mark the stream as a warning */
	string warning = "\033[1;43;37mWARNING:\033[0m \033[33m";
	for(auto ch : warning)
		os.put(ch);
	return os;
}

ostream &ERROR(ostream &os)
{
	/* IO Manipulator to mark the stream as an error */
	string error = "\033[1;41;37mERROR:\033[0m \033[31m";
	for(auto ch : error)
		os.put(ch);
	return os;
}
