/*
 * =====================================================================================
 *
 *       Filename:  stateMgr.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Saturday 20 December 2014 07:48:13  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/stateMgr.h"

#include <iostream>
#include <cstdlib>

using namespace std;

StateManager::StateManager()
{
    /* (w) create the basics which needs to be passed to the states */
    mWindow.create(sf::VideoMode(gmConfig.getWinResolution().first, gmConfig.getWinResolution().second), "gametest", sf::Style::Titlebar | sf::Style::Close);
    mWindow.setVerticalSyncEnabled(gmConfig.getVSync());
    hasLostFocus = false;
}

StateManager::~StateManager()
{
    /* (w) code will be added later */
}

void StateManager::pushState(GameState *inpState)
{
    mStateStack.push(inpState);
}

void StateManager::popState()
{
    if(!mStateStack.empty())
    {
        delete mStateStack.top();
        mStateStack.pop();
    }
}

void StateManager::changeState(GameState *inpState)
{
    popState();
    mStateStack.push(inpState);
}

GameState *StateManager::peekState()
{
    return (mStateStack.empty() ? nullptr:mStateStack.top());
}

void StateManager::gameLoop()
{
    sf::Clock mClock;
    sf::Time mLastFrameUpdate = sf::Time::Zero;

    const sf::Time timePerFrame = sf::seconds(1.f / 60.f);

    while(mWindow.isOpen())
    {
        /* (w) resize the window on resoluution change - without game restart */
        if((mWindow.getSize().x != (unsigned int)gmConfig.getWinResolution().first) && (mWindow.getSize().y != (unsigned int)gmConfig.getWinResolution().second))
            mWindow.create(sf::VideoMode(gmConfig.getWinResolution().first, gmConfig.getWinResolution().second), "gametest", sf::Style::Titlebar | sf::Style::Close);

        if(peekState() != nullptr)
        {
            mLastFrameUpdate += mClock.restart();
            while(mLastFrameUpdate > timePerFrame)
            {
                mLastFrameUpdate -= timePerFrame;
                peekState()->update(timePerFrame.asSeconds());
            }
            peekState()->draw(timePerFrame.asSeconds());
        }
        else
            LOG<<ERROR<<"No Game State found in stack"<<endl;
    }
}


void StateManager::assignState(GameState *inpState, bool delState)
{
    /* (w) first check if both the storage variables are empty or not -
     * if they are , just push the state - no occurence of that state exists
     * */

    /* (w) Seems like a minor error in the code exists - though till now no SIGSEGV has been sent
     * */
    if(mStateStack.empty() && mStateVector.empty())
    {
        LOG<<INFO<<"No state instance exists for the current input state\n";
        pushState(inpState);
    }

    /* (w) If either of them is not empty - that means that an instance might exist
     * So check for the identity of the instance - first check the vector before going in for the stack */
    bool foundInstVec = false; /* (w) by default the instance is not present */
    int indexCount = 0;
    if(!mStateVector.empty())
    {
        for(auto &iElem : mStateVector)
        {
            if(iElem->getIdent() == inpState->getIdent())
            {
                /* (w) if the input state needs to be deleted, remove it from the vector here and push in a new state, else push the existing state */
                if(delState)
                    pushState(inpState);
                else
                    pushState(iElem);
                mStateVector.erase(mStateVector.begin() + indexCount);
                foundInstVec = true;
                break;
            }
            indexCount++;
        }
    }

    if(foundInstVec)
        LOG<<INFO<<"Found instance of inpState in vector\n";

    /* (w) the state instance has not been found in the vector - check for the same in the stack
     * if the none of the indexes match up, just push, else pop till it matches up */
    bool foundInstStack = false;

    if(!mStateStack.empty())
    {
        for(size_t index = 0; index < mStateStack.size() && !foundInstVec; index++)
        {
            /* (w) Changing certain portion of the code in here - seems like a small bug that exists */
            if(mStateStack.top()->getIdent() == inpState->getIdent())
            {
                LOG<<INFO<<"Found state - exiting the big loop\n";
                if(delState)
                {
                    mStateStack.pop();
                    pushState(inpState);
                }
                foundInstStack = true;
                break;
            }
            else
            {
                mStateVector.push_back(mStateStack.top());
                mStateStack.pop();
            }
        }
    }

    if(mStateStack.size() == 1 && !foundInstStack)
    {
        if(mStateStack.top()->getIdent() == inpState->getIdent())
        {
            if(delState)
            {
                mStateStack.pop();
                pushState(inpState);
            }
            LOG<<INFO<<"Found state - stack Size is only 1 - hence in another check\n";
            foundInstStack = true;
        }
    }

    if(!foundInstStack && !foundInstVec)
    {
        LOG<<INFO<<"No instance found in stack and vector - looks like a new call to a state, pushing directly\n";
        pushState(inpState);
    }
}
