/*
 * =====================================================================================
 *
 *       Filename:  sceneNode.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Friday 06 February 2015 10:55:07  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/sceneNode.h"
#include <iostream>
#include <cstdlib>

using namespace std;

scene::SceneGraph::SceneGraph()
{
    numLayers = 0;
}

scene::SceneGraph::~SceneGraph()
{
    /* (w) code will be added later */
}

void scene::SceneGraph::addSceneNode(int layer, const string &ID, SceneNode &sceneNode)
{
    if(layer > numLayers)
        throw EE("Layer is inaccessible");
    else if(layer == numLayers)
    {
        map<string, SceneNode*> tmpMap;
        tmpMap[ID] = &sceneNode;
        sceneInfoMap.push_back(tmpMap);
        numLayers++;
    }
    else
        sceneInfoMap[layer][ID] = &sceneNode;
}

void scene::SceneGraph::removeSceneNode(const string &ID)
{
    for(auto &iLayer : sceneInfoMap)
    {
        auto foundNode = iLayer.find(ID);
        if(foundNode != iLayer.end())
            iLayer.erase(foundNode);
    }
}

map<string, scene::SceneNode *>& scene::SceneGraph::returnLayer(int layer)
{
    if(layer >= numLayers)
        throw EE("Layer inaccessible");
    else
        return sceneInfoMap[layer];
}

int scene::SceneGraph::numOfLayers()
{
    return numLayers;
}

/* (w) Writing the sceneNode functions here  */
scene::SceneNode::SceneNode()
{
    mPosition.y = mPosition.x = DEFAULT_POS;
    nodeWidth = nodeHeight = DEFAULT_NODE_DIM;

    mSpriteSheet = nullptr;
    hasMoved = false;
}

std::pair<int, int> scene::SceneNode::getPosition()
{
    return make_pair(mSprite.getPosition().x, mSprite.getPosition().y);
}

void scene::SceneNode::setNodePosition(int row, int col)
{
    mPosition.x = row;
    mPosition.y = col;
    mSprite.setPosition(sf::Vector2f(row, col));
}

void scene::SceneNode::setNodeDimensions(int tileWidth, int tileHeight)
{
    nodeWidth = tileWidth;
    nodeHeight = tileHeight;
}
