/*
 * =====================================================================================
 *
 *       Filename:  gameConfig.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Thursday 09 April 2015 02:48:23  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/gameConfig.h"

GameConfig::GameConfig()
{
    /* (w) first check if the configuration file exists or not */
    if(fileHwInst.fileExists(SETTINGS_FILE))
    {
        LOG<<INFO<<"Settings configuration file found\n";
        /* (w) the file exists, so just read and populate the values */
        populateVector(GSETTINGSR);
    }
    else
    {
        LOG<<WARNING<<"Settings configuration file not found\n";

        /* (w) Default values have been added to this portion - will be reset after reading the configuration file */
        mWinResoHeight = DEFAULT_WIN_HEIGHT;
        mWinResoWidth = DEFAULT_WIN_WIDTH;

        isFullScr = false; /* (w) default setting would be false */
        vSyncEnabled = false;
        mMusVol = DEFAULT_MUS_VOL;
        mSfxVol = DEFAULT_SFX_VOL;

        populateVector(GSETTINGSW);
    }
}

GameConfig::~GameConfig()
{
    /* (w) default destructor, code will be added later */
}

void GameConfig::checkConfig()
{
    /* (w) this function is working at the moment, will make changes in this function later
     * This will be the main function for reading the data off the vector present in config header and then populating the variables in the GameConfig class */

    /* (w) the flow from this function will go to the fileIO class */
}

void GameConfig::populateVector(int mode)
{
    /* (w) this function needs to go through a lot of changes */
    //for(auto iter = 0; iter < (int)GSETTINGS::GSMEMCOUNT; ++iter)
        //this->configVec.push_back(std::make_pair(iter, iter + 1)); /* (w) Set might be required to be written for this portion, plus the implementa
        //-tion needs to be added */

    //fileHwInst.fileWrite(SETTINGS_FILE, configVec); /* (w) removing mode from the FileIO functions - thereby keeping it completely independent */
    //fileHwInst.fileRead(SETTINGS_FILE, configVec);


    /* (w) now restructuring the code in this function - currently trying to fix for the file read function only */
    int offset = 0;
    switch(mode)
    {
        case (int)MODES::GSETTINGSR:
            offset = (int)GSETTINGS::WRESWIDTH;
            configVec.clear();
            for(auto iter = 0; iter < (int)GSETTINGS::GSMEMCOUNT; ++iter)
                configVec.push_back(make_pair(offset + iter, 0)); /* (w) because the second value would be read from the file */

            fileHwInst.fileRead(SETTINGS_FILE, configVec);

            /* (w) Will only be called at the time for reading any of the files*/
            parseVec();
            break;

        /* (w) Write other modes for populating vector */
        case (int)MODES::GSETTINGSW:
            offset = (int)GSETTINGS::WRESWIDTH;
            configVec.clear();
            for(auto iter = 0; iter < (int)GSETTINGS::GSMEMCOUNT; ++iter)
                configVec.push_back(make_pair(offset + iter, 0));

            setValVec();
            /* (w) after the population, call the write function */
            fileHwInst.fileWrite(SETTINGS_FILE, configVec); /* (w) now to check the file that is written in the disk */
            break;

        case (int)MODES::INVENW: /* (w) remove and move to InvenConfig */
            /* (w) code will be added shortly */
            break;

        case (int)MODES::INVENR: /* (w) remove and move to InvenConfig */
            /* (w) code will be added shortly */
            break;

        default:
            break;
    }
}

void GameConfig::parseVec()
{
    for(auto &iElem : configVec)
    {
        switch(iElem.first)
        {
            case (int)GSETTINGS::WRESWIDTH:
                setWinResWidth(iElem.second); /* (w) the set functions need to be changed a lot now */
                break;

            case (int)GSETTINGS::WRESHEIGHT:
                setWinResHeight(iElem.second);
                break;

            case (int)GSETTINGS::FULLSCR:
                setFullScr(iElem.second);
                break;

            case (int)GSETTINGS::VSYNCENABLED:
                setVSync(iElem.second);
                break;

            /* (w) Need to write the portion for hndling the value of the music and
             * SFX volumes */

            case (int)GSETTINGS::MUSVOL:
                setMusVol(iElem.second);
                break;

            case (int)GSETTINGS::SFXVOL:
                setSfxVol(iElem.second);
                break;

            default:
                break;
        }
    }
}

void GameConfig::setValVec()
{
    /* (w) Make sure that the configVec has the first integer values always present */
    //double someVal;
    for(auto iter = 0; iter < (int)GSETTINGS::GSMEMCOUNT; ++iter)
    {
        switch(configVec[iter].first)
        {
            case (int)GSETTINGS::WRESWIDTH:
                configVec[iter] = make_pair(configVec[iter].first, getWinResolution().first);
                break;

            case (int)GSETTINGS::WRESHEIGHT:
                configVec[iter] = make_pair(configVec[iter].first, getWinResolution().second);
                break;

            case (int)GSETTINGS::FULLSCR:
                if(isFullScr)
                    configVec[iter] = make_pair(configVec[iter].first, 1);
                else
                    configVec[iter] = make_pair(configVec[iter].first, 0);
                break;

            case (int)GSETTINGS::VSYNCENABLED:
                if(vSyncEnabled)
                    configVec[iter] = make_pair(configVec[iter].first, 1);
                else
                    configVec[iter] = make_pair(configVec[iter].first, 0);
                break;

            case (int)GSETTINGS::MUSVOL:
                configVec[iter] = make_pair(configVec[iter].first, mMusVol);
                break;

            case (int)GSETTINGS::SFXVOL:
                configVec[iter] = make_pair(configVec[iter].first, mSfxVol);
                break;

            default:
                break;
        }
    }
}

/* (w) Most of the set functions are here - next are the get functions */

void GameConfig::setMusVol(double inpVal)
{
    mMusVol = inpVal;
}

void GameConfig::setSfxVol(double inpVal)
{
    mSfxVol = inpVal;
}

void GameConfig::setWinResWidth(double inpVal)
{
    mWinResoWidth = inpVal;
}

void GameConfig::setWinResHeight(double inpVal)
{
    mWinResoHeight = inpVal;
}

void GameConfig::setWinResolution(int inpWdRes, int inpHtRes)
{
    mWinResoWidth = inpWdRes;
    mWinResoHeight = inpHtRes;
}

void GameConfig::setVSync(int inpVal)
{
    if(inpVal)
        vSyncEnabled = true;
    else
        vSyncEnabled = false;
}

void GameConfig::setFullScr(int inpVal)
{
    if(inpVal)
        isFullScr = true;
    else
        isFullScr = false;
}

/* (w) the get functions will be written below here */

bool GameConfig::getVSync()
{
    return vSyncEnabled;
}

bool GameConfig::getFullScr()
{
    return isFullScr;
}

double GameConfig::getMusVol()
{
    return mMusVol;
}

double GameConfig::getSfxVol()
{
    return mSfxVol;
}

std::pair<int, int> GameConfig::getWinResolution()
{
    return make_pair(mWinResoWidth, mWinResoHeight);
}

vector<sf::VideoMode> GameConfig::fullScrVidModes() /* (w) Need to change the return type from void to vector<sf::VideoMode> */
{
    /* (w) code will be added shortly */
    vector<sf::VideoMode> modes = sf::VideoMode::getFullscreenModes();
    vector<sf::VideoMode> refinedModes;
    set<sf::VideoMode> rModes;

    for(auto &iElem : modes)
    {
        auto modePresent = false; /* (w) assuming that the mode is not present in the rModes set at the start */

        if(rModes.empty())
            rModes.insert(iElem);
        else
        {
            for(auto rmIter = rModes.begin(); rmIter != rModes.end(); ++rmIter)
            {
                if((rmIter->width == iElem.width) && (rmIter->height == iElem.height))
                {
                    modePresent = true;
                    break; /* (w) if it is a match, break out */
                }
            }
        }
        if(!modePresent)
            rModes.insert(iElem);
    }

    refinedModes.clear();
    for(auto &accIter : rModes)
        refinedModes.push_back(accIter);

    /* (w) changing the return type of this function */
    return refinedModes;
}

pair<unsigned int, unsigned int> GameConfig::circResolution(unsigned int resoWidth, unsigned int resoHeight, bool choice)
{
    vector<sf::VideoMode> tempVec = fullScrVidModes();
    pair<unsigned int, unsigned int> retResolution;
    auto foundReso = false; /* (w) assuming that the resolution has not been found */
    auto fnIndex = 0;

    for(auto iElem = tempVec.begin(); iElem != tempVec.end(); ++iElem)
    {
        if((iElem->width == resoWidth) && (iElem->height == resoHeight))
        {
            foundReso = true;
            if(choice)
                if(iElem - tempVec.begin() == 4)
                    fnIndex = tempVec.size() - tempVec.size();
                else
                    fnIndex = iElem - tempVec.begin() + 1;
            else
                if(iElem - tempVec.begin() == 0)
                    fnIndex = tempVec.size() - 1;
                else
                    fnIndex = iElem - tempVec.begin() - 1;
            break;
        }
    }

    if(foundReso)
        retResolution = make_pair(tempVec[fnIndex].width, tempVec[fnIndex].height);

    return retResolution;
}
