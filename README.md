# TP Test : Test game project
This is a test game project. Basic aim is to create a game along with an inhouse basic engine. As of now, the idea is to create an adventure game, though the dynamics of the same as well as gameplay has not been designed. This will be an Open source project and the game will be dedicated to the basic 2D genre of classic games like Contra, Mario, Metroid and the like.

# Current status
+ The engine is still under development as is the game, though the state handling is completed

# Programmers
+ nb0dy(NB) : Main basic code base, Logger module, Exception module, Basic gravity code implementation
+ ani(AK) : Basic state handling migration from Laurent Gomila's code base
+ war10ck(SB) : Multiple state handler function, Additional programming

# Changelog
+ Added velocity to the character [04/07]
+ Part fix of movement for character [04/07]

Please feel free to make suggestions for code changes and errors in coding standards.
