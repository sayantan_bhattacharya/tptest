#ifndef _GAMECONFIG_H
#define _GAMECONFIG_H

#include "log.h"
#include "config.h"
#include "fileIO.h"
#include "globals.h"
#include <cmath>
#include <set>
#include <SFML/Graphics.hpp>

#define DEFAULT_WIN_WIDTH 800
#define DEFAULT_WIN_HEIGHT 600
#define DEFAULT_MUS_VOL 1.0
#define DEFAULT_SFX_VOL 1.0

/* (w) Create a base class for this class - which will have a vector<int, int> and enums will be placed over here */

class GameConfig : public Config
{
    private:
        int mWinResoWidth, mWinResoHeight;
        bool isFullScr, vSyncEnabled;
        double mMusVol, mSfxVol;
        FileIO fileHwInst;

    private:
        void parseVec();
        void setWinResWidth(double inpVal);
        void setWinResHeight(double inpVal);
        void setValVec(); /* (w) I most definitely don't require the offset to be passed */

        std::vector<sf::VideoMode> fullScrVidModes();

    public:
        GameConfig();
        ~GameConfig();

        void setVSync(int inpVal);
        void setFullScr(int inpVal);

        void setWinResolution(int inpWdRes, int inpHtRes);
        void checkConfig(); /* (w) this function will be elaborated later */
        void populateVector(int mode);

        /* (w) the setting functions will mostly be called in the constructor for setting up the values - as of now, the default values have been set up
         * If no configuration file is found, one would be created with the deault values written and then the same will be shown in the options State */

        void setMusVol(double inpVal);
        void setSfxVol(double inpVal);

        /* (w) Option state helper functions for game configuration */
        std::pair<unsigned int, unsigned int> circResolution(unsigned int resoWidth, unsigned int resoHeight, bool choice);


        /* (w) the get functions */
        bool getVSync();
        bool getFullScr();
        double getMusVol();
        double getSfxVol();
        std::pair<int, int> getWinResolution();
};

#endif
