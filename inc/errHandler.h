/*
 * =====================================================================================
 *
 *       Filename:  errHandler.hpp
 *
 *    Description:  This header declares some throw-able exception objects
 *
 *        Version:  1.0
 *        Created:  12/10/2014 11:25:49 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Nilangshu Bidyanta (), nbidyanta@gmail.com
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef __ERRHANDLER_HPP
#define __ERRHANDLER_HPP

#include <exception>
#include <stdexcept>
#include <string>

using namespace std;

class ExitException : public exception
{
	/* This exception exits the program on being thrown */
	private:
		string what_arg;

	public:
		ExitException();
		ExitException(const char* msg, int line,
					  const char* file, const char* func);
		virtual const char* what() const noexcept;
};

#define EE(x) ExitException((x), __LINE__, __FILE__, __FUNCTION__)

#endif
