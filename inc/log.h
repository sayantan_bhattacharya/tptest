/*
 * =====================================================================================
 *
 *       Filename:  log.hpp
 *
 *    Description:  Header for logging utilities
 *
 *        Version:  1.0
 *        Created:  12/06/2014 11:44:24 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Nilangshu Bidyanta (), nbidyanta@gmail.com
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef __LOG_H
#define __LOG_H

#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class logDump : public ostream
{
	private:
		ofstream outFile;
		string fName;

	public:
		logDump();
		logDump(const string &fileName);
		~logDump();

		void setFile(const string &fileName);

		/* Handle manipulators */
		logDump &operator<<(ostream &(*manip)(ostream &));

		/* Handle all other values */
		template<class T> logDump &operator<<(const T &output);

};

extern logDump LOG;

// Declare some useful manipulators
ostream &INFO(ostream &os);
ostream &WARNING(ostream &os);
ostream &ERROR(ostream &os);
ostream &STD(ostream &os);

// Templated functions need to be implemented in the
// same place they are declared
template<class T>
logDump &logDump::operator<<(const T &output)
{
	cout<<output;
	if(outFile.is_open())
		outFile<<output;
	return (*this);
}

#endif
