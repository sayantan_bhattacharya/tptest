#ifndef _SCENENODE_H
#define _SCENENODE_H

#include <SFML/Graphics.hpp>
#include <vector>
#include <map>
#include <string>
#include "log.h"
#include "errHandler.h"
#include "animHandler.h"

#define DEFAULT_POS 10.f
#define DEFAULT_NODE_DIM 64

namespace scene
{
    class SceneNode : public sf::Drawable
    {
        protected:
            const sf::Texture *mSpriteSheet;
            sf::Sprite mSprite;
            AnimHandler mAnimHandler;
            sf::Vector2f mPosition;
            int nodeWidth, nodeHeight;
            std::map<std::string, Animation> animDatabase;

        public:
            bool hasMoved;

        protected:
            virtual void draw(sf::RenderTarget &inpTarget, sf::RenderStates states) const = 0;

        public:
            SceneNode();
            std::pair<int, int> getPosition();

            /* (w) all virtual functions go here */
            virtual void loadSpriteSheet(const sf::Texture &inpTexture) = 0;
            virtual void update(float dt) = 0;
            virtual ~SceneNode() {};

            /* (w) some more functions to set the sprites on the screen */
            void setNodeDimensions(int tileWidth, int tileHeight); /* (w) Now to write the function body */
            void setNodePosition(int row, int col);
    };

    class SceneGraph
    {
        private:
            std::vector<std::map<std::string, SceneNode*>> sceneInfoMap;
            int numLayers;

        public:
            SceneGraph();
            ~SceneGraph();

            void addSceneNode(int, const std::string &, SceneNode &);
            void removeSceneNode(const std::string &);
            map<std::string, SceneNode *>& returnLayer(int);

            int numOfLayers();
    };
}

#endif
