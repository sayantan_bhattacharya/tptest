#ifndef _MENUSTATE_H
#define _MENUSTATE_H

#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include "gameState.h"
#include "stateMgr.h"
#include "log.h"
#include "playState.h"
#include "optionState.h"

enum stateTypes
{
    CONTINUE,
    NEW_GAME,
    OPTIONS,
    CREDITS,
    QUIT
};

#define NUMOPTIONS 5

class MenuState : public GameState
{
    private:
        sf::Texture menuStateBg;    /* (w) menu state background */
        sf::Sprite menuStateBgSprite; /* (w) problematic definition */
        sf::Font menuStateFont;
        sf::Text menuStateOptText;  /* (w) Menu state text options, later to be removed from the whole code */
        std::vector<std::pair<stateTypes, std::string>> menuStateOptions;
        static int stateChoice;
        int identity;

    private:
        void loadMenuOptions();
        void displayMenuOptions();

    public:
        MenuState(StateManager &);

        void loadTexture();
        void loadFont(const std::string&);
        void draw(float dt);
        void update(float dt);
        void handleinput(sf::Event::KeyEvent&, bool);
        int getIdent();
};

#endif
