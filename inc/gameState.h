#ifndef _GAMESTATE_H
#define _GAMESTATE_H

#include "stateMgr.h"

class StateManager;
class GameState
{
    public:
        StateManager *stMgr;
    public:
        virtual void draw(const float dt) = 0;
        virtual void update(const float dt) = 0;
        virtual void handleinput(sf::Event::KeyEvent&, bool) = 0;
        virtual int getIdent() = 0;
    public:
        /* (w) Pure virtual destructors must be implemented - learnt from nb0dy - need to know the reason` */
        virtual ~GameState(){};
};

#endif
