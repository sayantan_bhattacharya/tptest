#ifndef _TITLESTATE_H
#define _TITLESTATE_H

#include <SFML/Graphics.hpp>
#include <unistd.h>
#include <string>
#include "gameState.h"
#include "menuState.h"
#include "stateMgr.h"
#include "log.h"

enum titleStateTypes
{
    BOFH,
    GAMENAME
};

#define ALPHAOFFSET 64

class TitleState : public GameState
{
    /* (w) body will be added here */
    private:
        sf::Sprite titleStateSprite;
        sf::Texture titleStateTexture;
        sf::Font titleStateCoFont;  /* (w) studio name font */
        sf::Text titleStateCoText; /* (w) studio and game name text */
        bool inc, dec, loadMenu, displayName;
        int alphaVal;
        int identity;   /* (w) the specific identity of the state - store in the contructor - return in getIdent */

    private:
        void loadTexture();
        void loadFont(const std::string&);
        void fadeTitle();

    public:
        TitleState(StateManager &);

        /* (w) other functions that need to be implemented from the pure virtual class */
        void draw(float dt);
        void update(float dt);
        void handleinput(sf::Event::KeyEvent&, bool);
        int getIdent();
};

#endif
