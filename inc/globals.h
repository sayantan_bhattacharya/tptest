#ifndef _GLOBALS_H
#define _GLOBALS_H

enum class DIRECTION
{
    NORTH,
    EAST,
    WEST,
    SOUTH
};

#define SETTINGS_FILE "gConfig.bin"
#define PROFILE_FILE "base.profile"
#define INVEN_FILE "inventory.bin"

enum class GSETTINGS /* (w) Members need to changed - because now pair<int, double> */
{
    WRESWIDTH = 100,
    WRESHEIGHT,
    FULLSCR,
    VSYNCENABLED,
    MUSVOL,
    SFXVOL,
    GSMEMCOUNT = 6, /* (w) member count */
};

enum MODES /* (w) Most probably this will be required only for the read part, but anyway */
{
    GSETTINGSW = 200, /* (w) game settings write */
    GSETTINGSR,       /* (w) game settings read */
    PROFILEW,         /* (w) profile write */
    PROFILER,         /* (w) profile read */
    INVENW,           /* (w) inventory write */
    INVENR            /* (w) inventory read */
};

enum PROFILE
{
    /* (w) Some more values will be added to this enum - this is the game save after all */
    HEALTH = 300,
    PLAYERLOCX, /* (w) screen blit x coordinate */
    PLAYERLOCY, /* (w) screen blit y coordinate */
    PLAYERLEVEL,
    PROFMEMCOUNT = 4 /* (w) member count */
};

enum GLEVEL /* (w) Subject to change later */
{
    LEVELA = 1,
    LEVELB,
    LEVELC,
    LEVELD,
    LEVELCOUNT = 4
};

#define DEFAULT_CHARACTER_SIZE 24
#define DEFAULT_TEXTPOS 100.f
#define DEFAULT_TEXT_SEP 26
#define MAX_HEALTH 100
#define DEFAULT_PLAYER_LOCX 100
#define DEFAULT_PLAYER_LOCY 100
#define PROFILEFND 1000
#define PROFILENFND 2000

#endif
