#ifndef _TEXTUREBANK_H
#define _TEXTUREBANK_H

#include <SFML/Graphics.hpp>
#include <map>
#include <string>
#include "log.h"
#include "errHandler.h"

class TextureBank
{
    private:
        std::map<std::string, sf::Texture *> textureStore;

    public:
        TextureBank();
        ~TextureBank();

        sf::Texture &getTexture(const std::string &);
        void setTextureFromFile(const std::string &, const std::string &);
};

#endif
