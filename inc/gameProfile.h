#ifndef _GAMEPROFILE_H
#define _GAMEPROFILE_H

#include <SFML/Graphics.hpp>
#include "log.h"
#include "globals.h"
#include "fileIO.h"
#include "config.h"

class GameProfile : public Config
{
    private:
        FileIO fileHandle;
        long pHealth;
        int mPlayerLocX, mPlayerLocY, mPlayerLevel;

    private:
        void setValVec();
        void parseVec();

    public:
        bool profileExists;

    public:
        GameProfile();
        /* (w) might also require a parameterized constructor */
        ~GameProfile();

        void checkProfile(bool resetSave = false);

        void populateVector(int mode);

        void setHealth(long inpHealth);
        void setLocation(std::pair<int, int> inpLocPair);
        void setLocation(int inpLocX, int inpLocY);
        void setLocationX(int inpLocX);
        void setLocationY(int inpLocY);
        void setLevel(int inpLevel);

        long getHealth();
        std::pair<int, int> getLocation(); /* (w) this location one will always return a pair */
        int getLevel();
};

#endif
