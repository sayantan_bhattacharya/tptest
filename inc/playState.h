#ifndef _PLAYSTATE_H
#define _PLAYSTATE_H

#include <SFML/Graphics.hpp>
#include "stateMgr.h"
#include "gameState.h"
#include "log.h"
#include "menuState.h"
#include "world.h"
#include "sceneNode.h"

class PlayState : public GameState
{
    private:
        World world;
        int identity;

        /* (w) Wondering whether to create a view or not */

    public:
        PlayState(StateManager &);
        ~PlayState();

        /* (w) other functions that need to be overloaded */
        void loadTexture(); /* (w) will be written later - most probably will be moved to another module - decision to be done later */
        void draw(float dt);
        void update(float dt);
        void handleinput(sf::Event::KeyEvent&, bool);
        int getIdent();
};

#endif
