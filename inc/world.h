#ifndef _WORLD_H
#define _WORLD_H

#include <SFML/Graphics.hpp>
#include <utility>
#include "stateMgr.h"
#include "log.h"
#include "textureBank.h"
#include "playerUnit.h"
#include "sceneNode.h"

/* (w) TODO : Also need to check why the animation database is empty, check code flow */
class World
{
    private:
        StateManager *stMgr;
        TextureBank worldTextureBankInst; /* (w) will hold only the world textures - textures will be loaded using the search level process */
        PlayerUnit puMain; /* (w) main player unit instance */
        scene::SceneGraph scGraph; /* (w) The whole scene info handler */
        const int tileDim; /* (w) tile dimension for the sprite tiles to be loaded */

    public:
        World(StateManager &);
        ~World();

        void handleinput(sf::Event::KeyEvent &, bool);
        void handleKeysRealTime(float dt);

        void draw(float dt); /* (w) Need to write the function body */
        void update(float dt);

        void loadTexture();
};

#endif
