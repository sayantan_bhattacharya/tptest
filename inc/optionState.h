#ifndef _OPTIONSTATE_H
#define _OPTIONSTATE_H

#include <SFML/Graphics.hpp>
#include "gameState.h"
#include "stateMgr.h"
#include "log.h"
#include "menuState.h"
#include "gameConfig.h"
#include <string>
#include <vector>

/* (w) Write the rest of the virtual function implementation here
 * Also have to find out how to perform the doubleBuffer and tripleBuffer implementation */

/* (w) DONE : Write arrow key handling routines for the optionState - Up and down completed, Left and right remaining
 * DONE : Write the body of the displayOptions() function
 * DONE: Write loadFont() function
 * DONE : Up and Down arrow key handling routine completed
 * DONE : Write routines for Left and Right arrow key
 * TODO : Write the toast message function for updating user about resolution changes and to restart the game for the resolution to take effect */

enum CONFIGOPTIONS
{
    /* (w) will be added later */
    RESOLUTION,
    FULLSCREEN,
    VIDEOSYNC,
    MUSICVOL,
    SOUNDFXVOL
};

#define STEP 0.2

class OptionState : public GameState
{
    private:
        int identity;
        sf::Texture optionStateBg; /* (w) Option state background image */
        sf::Sprite optionStateBgSprite; /* (w) another problematic definition */
        sf::Font optionStateFont;
        sf::Text optionStateText;
        GameConfig mConfigInst; /* (w) not sure whether this will be required anymore or not */
        std::vector<std::pair<int, std::string>> optStateDispData;
        static int stateChoice;

    private:
        void readConfigFile(); /* (w) Will be freezecoding the config file name */
        void saveConfigFile(); /* (w) save the config file with the recent data that has been changed */
        void loadOptionsText();
        void displayOptions();
        void cycleOptionVal(bool choice);

    public:
        OptionState(StateManager &);
        ~OptionState();

    public:
        /* (w) Virtual function declaration will be written here */
        void draw(float dt);
        void update(float dt);
        void handleinput(sf::Event::KeyEvent&, bool);
        int getIdent();

        /* (w) other optionState specific functions to be implemented below here */
        void loadTexture(); /* (w) currently will contain the resource file names hardcoded */
        void loadFont(const std::string& fontFilePath);
};
#endif
