#ifndef _STATEMGR_H
#define _STATEMGR_H

#include <SFML/Graphics.hpp>
#include <stack>
#include <vector>

#include "gameState.h"
#include "log.h"
#include "gameConfig.h"
#include "globals.h"
#include "gameProfile.h"

enum states
{
    TITILESTATE,
    MENUSTATE,
    OPTIONSTATE,
    PLAYSTATE
};

class GameState;
class StateManager
{
    public:
        sf::RenderWindow mWindow;
        std::stack<GameState *> mStateStack;
        std::vector<GameState*> mStateVector;

        bool hasLostFocus;
        GameConfig gmConfig;
        GameProfile gmProfile; /* (w) Hope this one works out */

    public:
        StateManager();
        ~StateManager();

        void pushState(GameState *);
        void popState();
        void changeState(GameState *);
        GameState *peekState();

        void assignState(GameState *inpState, bool delState = false);

        void gameLoop();
};

#endif
