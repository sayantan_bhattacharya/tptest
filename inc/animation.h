#ifndef _ANIMATION_H
#define _ANIMATION_H

#include <SFML/Graphics.hpp>
#include <vector>
#include "errHandler.h"

/* (w) write the function bodies for the member functions */

class Animation
{
    private:
        std::vector<sf::IntRect> mframes;
        size_t numFrames, curFrame;
        float duration, timeElapsed;
        bool loop; /* (w) to set the looping animation */

    public:
        Animation();
        ~Animation();

        void addFrame(const sf::IntRect &inFrame);
        void setDuration(float desDuration);
        void setLooping(bool l); /* (w) almost blindly copying nb0dy's code - only thing is I am trying to understand how he made it to work */
        void resetAnimation();

        sf::IntRect getFrame(size_t frameNum);
        sf::IntRect getCurFrame();
        void updateFrame(float dt);
};
#endif
