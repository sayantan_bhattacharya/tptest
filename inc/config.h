#ifndef _CONFIG_H
#define _CONFIG_H

#include <vector>

enum configTypes
{
    VSYNC = 100, /* (w) bool */
    HRESO, /* (w) int */
    VRESO, /* (w) int */
    FLSCR, /* (w) bool */
    MUSVOL, /* (w) double */
    SFXVOL /* (w) double */
};

class Config
{
    public:
        std::vector<std::pair<int, double>> configVec;

    public:
        virtual void populateVector(int mode) = 0;
        virtual ~Config(){};
};

#endif
