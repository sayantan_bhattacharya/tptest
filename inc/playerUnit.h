#ifndef _PLAYERUNIT_H
#define _PLAYERUNIT_H

#include <SFML/Graphics.hpp>
#include "log.h"
#include "sceneNode.h"

/* (w) Inheriting the scene::SceneNode class is giving compile problems - have to check that now - draw function needs to be implemented */

/* (w) States for the character, will have only left and right */
enum class UnitState
{
    WALK_R,
    FACE_R,
    WALK_L,
    FACE_L
};

class PlayerUnit : public scene::SceneNode
{
    private:
        float puHealth, puAccel, puSpeed; /* (w) basic player unit attributes */
        sf::Vector2f mVel;
        UnitState mState;

    private:
        /* (w) private functions go here */
        void draw(sf::RenderTarget &inpTarget, sf::RenderStates states) const;
        void generateAnimations();

        /* (w) not applying jump animation and graviity at the moment */

    public:
        PlayerUnit();
        ~PlayerUnit();
        void handleinput(sf::Event::KeyEvent &, bool);

        /* (w) Need to write the handleKeysRealtime function */
        void handleKeysRealTime(float dt);

        /* (w) virtual function implementation */
        void loadSpriteSheet(const sf::Texture &inpTexture);
        void update(float dt);
};

#endif
