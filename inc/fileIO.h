#ifndef _FILEIO_H
#define _FILEIO_H

#include "log.h"
#include <string>
#include <vector>
#include <sys/stat.h> /* (w) writing only for POSIX abiding OSes */
#include <fstream>

/* (w) The following will be structure of all files being written by the game program
 * 1. All the files will have data in the following fashion
 *      vsync : 1/0 -- 1 represents ON state and 0 represents OFF state
 * 2. The parser will parse these lines and send the values - the parser will not be required any more */

class FileIO
{
    private:
        std::vector<std::string> lineInfo; /* (w) per line to be stored in here for each fileRead call - decrypted line to be pushed_back */

    private: /* (w) need to check the encoding and decoding functions */
        std::string b64Encode(unsigned char const*, unsigned int len);
        std::string b64Decode(const std::string &str);

    public:
        FileIO();
        ~FileIO();

    public:
        void fileWrite(const std::string fileName, std::vector<pair<int, double>> &inpConfVec); /* (w) Provide the filename along with the relative/absolute path */
        void fileRead(const std::string fileName, std::vector<pair<int, double>> &inpConfVec); /* (w) Will be reading the file and storing the data line by line in a vector */
        bool fileExists(const std::string &inpConfigFile);
};

#endif
