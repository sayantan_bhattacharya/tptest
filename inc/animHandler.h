#ifndef _ANIMHANDLER_H
#define _ANIMHANDLER_H

#include "animation.h"

typedef  map<std::string, Animation>* AnimDb; /* (w) Will be holding the animation objects here */

class AnimHandler
{
    private:
        AnimDb mAnim;
        size_t numAnim;
        sf::Sprite *mUnitprite;
        string curAnim;
        bool animStopped, manFrameSet;

    public:
        AnimHandler();
        ~AnimHandler();

        AnimHandler(sf::Sprite *sp, AnimDb inpAnimDb);
        void setTargetSprite(sf::Sprite *inpSprite);
        void setAnimList(AnimDb inpAnimDb);
        void setCurAnim(const string &animOffset);
        void setAnimFrame(size_t inpFrame);

        void update(float dt);
        void stopAnim();
        void startAnim();
};
#endif
