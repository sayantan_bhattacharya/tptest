# Makefile for game0
# (c) Nilangshu : nbidyanta@gmail.com
# used by war10ck

# Define flags
CC = g++
SRC_DIR = src
OBJ_DIR = obj
INC_DIR = inc
BUILD_DIR = bin
LIB_DIR = lib
DEBUG = -g
OFLAG = -O2
WFLAG = -w
LIB = -lsfml-window -lsfml-graphics -lsfml-system
CPP11 = -std=c++11
RPATH_OPT = -Wl,-rpath=/usr/lib64/
CFLAGS = $(DEBUG) $(WFLAG) -I $(INC_DIR) -L $(LIB_DIR) $(LIB) $(RPATH_OPT) #$(OFLAG)

$(shell mkdir -p obj bin)

# Main target name
EXEC = game0

SOURCES = $(shell find $(SRC_DIR)/ -name '*.cpp')
VPATH = $(dir $(SOURCES))
SOURCES_BASE = $(notdir $(SOURCES))

OBJECTS = $(patsubst %.cpp, $(OBJ_DIR)/%.o, $(SOURCES_BASE))

$(EXEC): $(OBJECTS)
	$(CC) $(CPP11) $(OBJECTS) -L $(LIB_DIR) $(LIB) $(RPATH_OPT) -o $(BUILD_DIR)/$(EXEC)

# Generic rule
$(OBJ_DIR)/%.o : %.cpp
	$(CC) $(CPP11) -c $(CFLAGS) $< -o $@

clean:
	rm -f $(OBJECTS) $(BUILD_DIR)/$(EXEC)
